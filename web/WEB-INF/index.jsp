<%@ page import="projet.data.Etudiant" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Bienvenue sur la gestion des notes et des absences</h3>
<ul>
    <li><a href="<%= application.getContextPath()%>/do/etudiants">Voir les étudiants</a></li>
    <li><a href="<%= application.getContextPath()%>/do/groupes">Voir les groupes</a></li>
</ul>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>