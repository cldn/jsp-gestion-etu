<%@ page contentType="text/html;charset=UTF-8" %>

            </div>
        </main>
    </body>
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col s6">
                    <h5 class="white-text">Projet MI4</h5>
                </div>
                <div class="col s6">
                    <h5 class="white-text right-align">2017-2018</h5>
                </div>
            </div>
        </div>
    </footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.sidenav').sidenav();
        });
    </script>

    <style>
        html, body {
            height: 100%;
        }

        body {
            display: flex;
            flex-direction: column;
        }

        body main, body main div.container {
            flex: 1 0 auto;
        }

        footer {
            margin-top: 1rem;
            flex-shrink: 0;
        }

        a {
            font-size: 1.2em;
        }
    </style>
</html>