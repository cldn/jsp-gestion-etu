<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><%= getServletConfig().getServletContext().getInitParameter("title")%></title>
    </head>

    <header>
        <nav>
            <div class="nav-wrapper">
                <a href="<%= application.getContextPath()%>/do/home" class="brand-logo right">Projet MI4</a>
                <a href="#" data-target="mobile-menu" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul id="nav-mobile" class="left hide-on-med-and-down">
                    <li><a href="<%= application.getContextPath() %>/do/home">Accueil</a></li>
                    <li><a href="<%= application.getContextPath() %>/do/groupes">Groupes</a></li>
                    <li><a href="<%= application.getContextPath() %>/do/etudiants">Étudiants</a></li>
                    <li><a href="<%= application.getContextPath() %>/do/absences">Absences</a></li>
                    <li><a href="<%= application.getContextPath() %>/do/modules">Modules</a></li>
                    <li><a href="<%= application.getContextPath() %>/do/notes">Notes</a></li>
                </ul>
            </div>
        </nav>

        <ul class="sidenav" id="mobile-menu">
            <li><a href="<%= application.getContextPath() %>/do/home">Accueil</a></li>
            <li><a href="<%= application.getContextPath() %>/do/groupes">Groupes</a></li>
            <li><a href="<%= application.getContextPath() %>/do/etudiants">Étudiants</a></li>
            <li><a href="<%= application.getContextPath() %>/do/absences">Absences</a></li>
            <li><a href="<%= application.getContextPath() %>/do/modules">Modules</a></li>
            <li><a href="<%= application.getContextPath() %>/do/notes">Notes</a></li>
        </ul>
    </header>

    <body>
        <main>
            <div class="container">
