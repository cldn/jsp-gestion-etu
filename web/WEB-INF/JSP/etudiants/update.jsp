<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="id" class="java.lang.String" scope="request"></jsp:useBean>

<h3>Modification - étudiant</h3>
<p>Les modifications ont bien été prises en compte.</p>

<a href="<%= application.getContextPath()%>/do/etudiant?id=<%=id %>">Retourner à la fiche de l'étudiant</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>