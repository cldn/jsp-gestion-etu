<%@ page import="projet.data.Groupe" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="groupes" type="java.util.List<projet.data.Groupe>" scope="request"></jsp:useBean>

<h3>Créer un étudiant</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/etudiant/new/post">

    <div class="input-field col s12">
        <input type="text" id="nom" name="nom" autofocus />
        <label for="nom">Nom</label>
    </div>

    <div class="input-field col s12">
        <input type="text" id="prenom" name="prenom" />
        <label for="prenom">Prénom</label>
    </div>

    <div class="input-field col s12">
        <select id="groupe" name="groupe">
            <% for(Groupe grp: groupes) { %>
                <option value="<%= grp.getId()%>"><%= grp.getNom()%></option>
            <% } %>
        </select>
        <label>Groupe</label>
    </div>

    <div class="input-field col s12">
        <input type="number" id="absences" name="absences" />
        <label for="absences">Absences</label>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>