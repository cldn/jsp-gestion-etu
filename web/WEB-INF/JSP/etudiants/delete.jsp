<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Suppression - étudiant</h3>
<p>L'étudiant a bien été supprimé.</p>

<a href="<%= application.getContextPath()%>/do/etudiants">Retourner à la liste des étudiants</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>