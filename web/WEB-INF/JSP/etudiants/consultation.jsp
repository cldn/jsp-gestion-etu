<%@ page import="projet.data.Etudiant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="etudiants" type="java.util.List<projet.data.Etudiant>" scope="request"></jsp:useBean>

<h3>Consultation - étudiants</h3>
<div class="collection">
    <%
        for(Etudiant etu: etudiants) {
    %>
    <a class="collection-item" href="<%= application.getContextPath() %>/do/etudiant?id=<%= etu.getId() %>">
        <%= etu.getPrenom() + " " + etu.getNom() %>
    </a>
    <% } %>
</div>

<a href="<%= application.getContextPath() %>/do/etudiant/new">Ajouter un étudiant</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>