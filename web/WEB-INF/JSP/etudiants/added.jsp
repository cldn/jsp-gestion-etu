<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="id" class="java.lang.String" scope="request"></jsp:useBean>

<h3>Ajout - étudiant</h3>
<p>L'étudiant a bien été créé.</p>

<a href="<%= application.getContextPath()%>/do/etudiant?id=<%=id %>">Consulter la fiche de l'étudiant</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>