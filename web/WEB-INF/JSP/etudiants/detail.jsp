<%@ page import="projet.data.Groupe" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="etudiant" class="projet.data.Etudiant" scope="request" />

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="groupes" type="java.util.List<projet.data.Groupe>" scope="request"></jsp:useBean>

<h3>Détails de l'étudiant</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/etudiant/update?id=<%= etudiant.getId() %>">

    <div class="input-field col s12">
        <input type="number" id="id" name="id" value="<jsp:getProperty name="etudiant" property="id" />" disabled />
        <label for="id">ID</label>
    </div>

    <div class="input-field col s12">
        <input type="text" id="nom" name="nom" value="<jsp:getProperty name="etudiant" property="nom" />" />
        <label for="nom">Nom</label>
    </div>

    <div class="input-field col s12">
        <input type="text" id="prenom" name="prenom" value="<jsp:getProperty name="etudiant" property="prenom" />" />
        <label for="prenom">Prénom</label>
    </div>

    <div class="input-field col s12">
        <select id="groupe" name="groupe">
            <%
                for(Groupe grp: groupes) {
                    if (etudiant.getGroupe().getId() == grp.getId()) {
            %>
            <option selected value="<%= grp.getId()%>"><%= grp.getNom()%></option>
            <%
            } else {
            %>
            <option value="<%= grp.getId()%>"><%= grp.getNom()%></option>
            <%
                    }
                }
            %>
        </select>
        <label>Groupe</label>
    </div>

    <div class="input-field col s12">
        <input type="number" id="absences" name="absences" value="<jsp:getProperty name="etudiant" property="nbAbsences" />" />
        <label for="absences">Absences</label>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<a class="waves-light waves-effect red btn" href="<%= application.getContextPath() %>/do/etudiant/delete?id=<%= etudiant.getId() %>">Supprimer</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>