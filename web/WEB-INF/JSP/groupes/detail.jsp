<%@ page import="projet.data.Etudiant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="groupe" class="projet.data.Groupe" scope="request" />

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Détails du groupe</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/groupe/update?id=<%= groupe.getId() %>">

    <label for="id">ID</label>
    <input type="number" id="id" name="id" value="<jsp:getProperty name="groupe" property="id" />" disabled />

    <label for="nom">Nom</label>
    <input type="text" id="nom" name="nom" value="<jsp:getProperty name="groupe" property="nom" />" />

    <p>Étudiants membres :</p>
    <div class="collection">
        <%
            for(Etudiant etu: groupe.getEtudiants()) {%>
        <a class="collection-item" href="<%= application.getContextPath()%>/do/etudiant?id=<%= etu.getId() %>">
            <%= etu.getPrenom() + " " + etu.getNom() %>
        </a>
        <% } %>
    </div>

    <button class="waves-light waves-effect btn" type="submit">Enregistrer</button>
</form>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />
