<%@ page import="projet.data.Etudiant" %>
<%@ page import="java.util.List" %>
<%@ page import="projet.data.Groupe" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="groupes" type="java.util.List<projet.data.Groupe>" scope="request"></jsp:useBean>

<h3>Consultation - groupes</h3>
<div class="collection">
    <%
        for(Groupe grp: groupes) {%>
    <a class="collection-item" href="<%= application.getContextPath()%>/do/groupe?id=<%= grp.getId() %>">
        #<%= grp.getId() + " - " + grp.getNom() %>
    </a>
    <% } %>
</div>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>