<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Suppression - module</h3>
<p>Le module a bien été supprimé.</p>

<a href="<%= application.getContextPath()%>/do/modules">Retourner à la liste des modules</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>