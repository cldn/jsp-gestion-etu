<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="module" class="projet.data.Module" scope="request" />

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Détails du module</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/module/update?id=<%= module.getId() %>">

    <div class="input-field col s12">
        <input type="number" id="id" name="id" value="<jsp:getProperty name="module" property="id" />" disabled />
        <label for="id">ID</label>
    </div>

    <div class="input-field col s12">
        <input type="text" id="name" name="name" value="<jsp:getProperty name="module" property="name" />" />
        <label for="name">Nom</label>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<a class="waves-light waves-effect red btn" href="<%= application.getContextPath() %>/do/module/delete?id=<%= module.getId() %>">Supprimer</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />