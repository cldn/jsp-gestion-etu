<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="id" class="java.lang.String" scope="request"></jsp:useBean>

<h3>Ajout - module</h3>
<p>Le module a bien été créé.</p>

<a href="<%= application.getContextPath()%>/do/module?id=<%=id %>">Consulter la fiche du module</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>