<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Créer un module</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/module/new/post">

    <div class="input-field col s12">
        <input type="text" id="nom" name="nom" autofocus />
        <label for="nom">Nom</label>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />