<%@ page import="projet.data.Module" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="modules" type="java.util.List<projet.data.Module>" scope="request"></jsp:useBean>

<h3>Consultation - modules</h3>
<div class="collection">
    <%
        for(Module mod: modules) {%>
    <a class="collection-item" href="<%= application.getContextPath()%>/do/module?id=<%= mod.getId() %>">
        <%= mod.getName() %>
    </a>
    <% } %>
</div>

<a href="<%= application.getContextPath() %>/do/module/new">Ajouter un module</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>