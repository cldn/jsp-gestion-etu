<%@ page import="projet.data.Etudiant" %>
<%@ page import="java.util.List" %>
<%@ page import="projet.data.Groupe" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="groupes" type="java.util.List<projet.data.Groupe>" scope="request"></jsp:useBean>

<h3>Consultation - absences</h3>
<%
    if (groupes.size() > 0) {
%>
<div class="groups">
    <% for(Groupe grp: groupes) { %>
    <h1><%= grp.getNom() %></h1>
    <div class="collection">
        <% for (Etudiant etu: grp.getEtudiants()) { %>
        <a class="collection-item" href="<%= application.getContextPath()%>/do/etudiant?id=<%= etu.getId() %>">
            <%= etu.getPrenom() + " " + etu.getNom() + " : " + etu.getNbAbsences() + " absences" %>
        </a>
        <% } %>
    </div>
    <% } %>
</div>
<% } else { %>
    <h5>Aucun étudiant avec des absences trouvé.</h5>
<% } %>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>