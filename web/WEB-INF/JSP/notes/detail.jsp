<%@ page import="projet.data.Etudiant" %>
<%@ page import="projet.data.Module" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="note" class="projet.data.Note" scope="request" />
<jsp:useBean id="etudiants" type="java.util.List<projet.data.Etudiant>" scope="request" />
<jsp:useBean id="modules" type="java.util.List<projet.data.Module>" scope="request" />

<h3>Détails de la note</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/note/update?id=<%= note.getId() %>">

    <div class="input-field col s12">
        <input type="number" id="id" name="id" value="<jsp:getProperty name="note" property="id" />" disabled />
        <label for="id">ID</label>
    </div>

    <div class="input-field col s12">
        <select id="etudiant" name="etudiant">
            <%
                for(Etudiant etu: etudiants) {
                    if (note.getEtudiant().getId().equals(etu.getId())) {
            %>
            <option selected value="<%= etu.getId()%>"><%= etu.getPrenom() + ' ' + etu.getNom()%></option>
            <%
            } else {
            %>
            <option value="<%= etu.getId()%>"><%= etu.getPrenom() + ' ' + etu.getNom()%></option>
            <%
                    }
                }
            %>
        </select>
        <label for="etudiant">Étudiant</label>
    </div>

    <div class="input-field col s12">
        <select id="module" name="module">
            <%
                for(Module mod: modules) {
                    if (note.getModule().getId().equals(mod.getId())) {
            %>
            <option selected value="<%= mod.getId()%>"><%=mod.getName()%></option>
            <%
            } else {
            %>
            <option value="<%= mod.getId()%>"><%= mod.getName()%></option>
            <%
                    }
                }
            %>
        </select>
        <label for="module">Module</label>
    </div>

    <div class="input-field col s12" style="margin-top: 2rem;">
        <p class="range-field">
            <label for="value">Valeur</label>
            <input type="range" id="value" name="value" min="0" max="20" step="0.1" value="<jsp:getProperty name="note" property="value" />"/>
        </p>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<a class="waves-light waves-effect red btn" href="<%= application.getContextPath() %>/do/note/delete?id=<%= note.getId() %>">Supprimer</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>