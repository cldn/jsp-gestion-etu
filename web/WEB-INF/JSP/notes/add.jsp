<%@ page import="projet.data.Etudiant" %>
<%@ page import="projet.data.Module" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="etudiants" type="java.util.List<projet.data.Etudiant>" scope="request"></jsp:useBean>
<jsp:useBean id="modules" type="java.util.List<projet.data.Module>" scope="request"></jsp:useBean>

<h3>Ajouter une note</h3>
<form method="post" action="<%= getServletConfig().getServletContext().getContextPath()%>/do/note/new/post">

    <div class="input-field col s12">
        <select id="etudiant" name="etudiant">
            <% for(Etudiant etu: etudiants) { %>
            <option value="<%= etu.getId()%>"><%= etu.getPrenom() + ' ' + etu.getNom()%></option>
            <% } %>
        </select>
        <label>Étudiant</label>
    </div>

    <div class="input-field col s12">
        <select id="module" name="module">
            <% for(Module mod: modules) { %>
            <option value="<%= mod.getId()%>"><%= mod.getName()%></option>
            <% } %>
        </select>
        <label>Module</label>
    </div>

    <div class="input-field col s12" style="margin-top: 2rem;">
        <p class="range-field">
            <label for="value">Valeur</label>
            <input type="range" id="value" name="value" min="0" max="20" step="0.1"/>
        </p>
    </div>

    <button class="waves-light waves-effect btn" name="form" type="submit">Enregistrer</button>
</form>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>" />

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });
</script>