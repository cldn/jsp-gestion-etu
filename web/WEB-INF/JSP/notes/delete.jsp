<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />

<h3>Suppression - note</h3>
<p>La note a bien été supprimée.</p>

<a href="<%= application.getContextPath()%>/do/notes">Retourner à la liste des notes</a>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>