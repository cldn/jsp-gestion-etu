<%@ page import="projet.data.GestionFactory" %>
<%@ page import="projet.data.Etudiant" %>
<%@ page import="projet.data.Note" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%-- Include header --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("header")%>" />
<jsp:useBean id="etudiants" type="java.util.List<projet.data.Etudiant>" scope="request"></jsp:useBean>

<h3>Gestion des notes</h3>

<%
    if (etudiants.size() > 0) {
%>
<div class="etus">
    <a href="<%= application.getContextPath() %>/do/note/new">Ajouter une note</a>
    <% for(Etudiant etu: etudiants) { %>
    <h5><%= etu.getPrenom() + ' ' + etu.getNom() %></h5>
    <% if (etu.getNotes().size() > 0) { %>
    <table class="highlight" style="margin-bottom: 2rem;">
        <thead>
        <tr>
            <th>Module</th>
            <th>Note</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% for (Note note: etu.getNotes()) { %>
        <tr>
            <td><%= note.getModule().getName() %></td>
            <td><%= note.getValue() %></td>
            <td><a href="<%= application.getContextPath()%>/do/note?id=<%= note.getId() %>">Modifier</a></td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <% } else {%>
    <p>Aucune note.</p>
    <% } %>
    <% } } else { %>
    <h5>Aucun étudiant avec des notes trouvé.</h5>
    <% } %>
</div>

<%-- Include footer --%>
<jsp:include page="<%= getServletConfig().getServletContext().getInitParameter("footer")%>"/>