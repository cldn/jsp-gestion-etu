package projet.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Note implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(nullable=false)
    private Float value;

    @ManyToOne
    private Module module;

    @ManyToOne
    private Etudiant etudiant;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }
}
