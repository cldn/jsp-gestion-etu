package projet.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet.data.*;

@SuppressWarnings("serial")
public class Controller extends HttpServlet {

    private String urlHome;
    private String urlEtudiants;
    private String urlDetailEtudiant;
    private String urlAddEtudiant;
    private String urlFormAddEtudiant;
    private String urlDeleteEtudiant;
    private String urlUpdateEtudiant;
    private String urlGroupes;
    private String urlDetailGroupe;
    private String urlUpdateGroupe;
    private String urlAbsences;
    private String urlModules;
    private String urlDetailModule;
    private String urlAddModule;
    private String urlFormAddModule;
    private String urlDeleteModule;
    private String urlUpdateModule;
    private String urlNotes;
    private String urlDetailNote;
    private String urlAddNote;
    private String urlFormAddNote;
    private String urlDeleteNote;
    private String urlUpdateNote;

    // INIT
    @Override
    public void init() throws ServletException {
        // Récupération des URLs en paramètre du web.xml
        urlHome = getServletConfig().getInitParameter("urlHome");
        urlEtudiants = getServletConfig().getInitParameter("urlEtudiants");
        urlAddEtudiant = getServletConfig().getInitParameter("urlAddEtudiant");
        urlFormAddEtudiant = getServletConfig().getInitParameter("urlFormAddEtudiant");
        urlDeleteEtudiant = getServletConfig().getInitParameter("urlDeleteEtudiant");
        urlDetailEtudiant = getServletConfig().getInitParameter("urlDetailEtudiant");
        urlUpdateEtudiant = getServletConfig().getInitParameter("urlUpdateEtudiant");
        urlGroupes = getServletConfig().getInitParameter("urlGroupes");
        urlDetailGroupe = getServletConfig().getInitParameter("urlDetailGroupe");
        urlUpdateGroupe = getServletConfig().getInitParameter("urlUpdateGroupe");
        urlAbsences = getServletConfig().getInitParameter("urlAbsences");
        urlModules = getServletConfig().getInitParameter("urlModules");
        urlAddModule = getServletConfig().getInitParameter("urlAddModule");
        urlFormAddModule = getServletConfig().getInitParameter("urlFormAddModule");
        urlDeleteModule = getServletConfig().getInitParameter("urlDeleteModule");
        urlDetailModule = getServletConfig().getInitParameter("urlDetailModule");
        urlUpdateModule = getServletConfig().getInitParameter("urlUpdateModule");
        urlNotes = getServletConfig().getInitParameter("urlNotes");
        urlAddNote = getServletConfig().getInitParameter("urlAddNote");
        urlFormAddNote = getServletConfig().getInitParameter("urlFormAddNote");
        urlDeleteNote = getServletConfig().getInitParameter("urlDeleteNote");
        urlDetailNote = getServletConfig().getInitParameter("urlDetailNote");
        urlUpdateNote = getServletConfig().getInitParameter("urlUpdateNote");

        // Uncomment this if you want to check all the declared init params in the servlet config
        /*
        Enumeration<String> s = getServletConfig().getInitParameterNames();
        while (s.hasMoreElements()) {
            String elt = s.nextElement();
            System.out.println(elt + " *** " + getServletConfig().getInitParameter(elt));
            RequestDispatcher dispatch = getServletContext().getRequestDispatcher(getServletConfig().getInitParameter(elt));
            System.out.println(dispatch == null ? "NULL" : dispatch);
        }
        */

        // Création de la factory permettant la création d'EntityManager
        // (gestion des transactions)
        GestionFactory.open();

        ///// INITIALISATION DE LA BD
        // Normalement l'initialisation se fait directement dans la base de données
        if ((GroupeDAO.getAll().size() == 0) && (EtudiantDAO.getAll().size() == 0)) {

            // Creation des groupes
            Groupe MIAM = GroupeDAO.create("miam");
            Groupe SIMO = GroupeDAO.create("SIMO");
            Groupe MESSI = GroupeDAO.create("MESSI");

            // Creation des étudiants
            EtudiantDAO.create("Francis", "Brunet-Manquat", MIAM, 0);
            EtudiantDAO.create("Philippe", "Martin", MIAM, 0);
            EtudiantDAO.create("Mario", "Cortes-Cornax", MIAM, 0);
            EtudiantDAO.create("Françoise", "Coat", SIMO, 0);
            EtudiantDAO.create("Laurent", "Bonnaud", MESSI, 0);
            EtudiantDAO.create("Sébastien", "Bourdon", MESSI, 0);
            EtudiantDAO.create("Mathieu", "Gatumel", SIMO, 0);
        }
    }

    @Override
    public void destroy() {
        super.destroy();

        // Fermeture de la factory
        GestionFactory.close();
    }

    // POST
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // on passe la main au GET
        doGet(request, response);
    }

    // GET
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        // On récupère le path
        String action = request.getPathInfo();
        if (action == null) {
            action = "/home";
        }

        System.out.println("Navigating to: " + action);

        // Exécution de l'action
        switch (action) {
            case "/home":
                doHome(request, response);

                // Etudiant routes
            case "/etudiants":
                doEtudiants(request, response);
            case "/etudiant":
                doDetailEtudiant(request, response);
            case "/etudiant/update":
                doUpdateEtudiant(request, response);
            case "/etudiant/new":
                doAddEtudiant(request, response);
            case "/etudiant/new/post":
                doFormAddEtudiant(request, response);
            case "/etudiant/delete":
                doDeleteEtudiant(request, response);

                // Groupe routes
            case "/groupes":
                doGroupes(request, response);
            case "/groupe":
                doDetailGroupe(request, response);
            case "/groupe/update":
                doUpdateGroupe(request, response);

                // Absence routes
            case "/absences":
                doAbsences(request, response);

                // Module routes
            case "/modules":
                doModules(request, response);
            case "/module":
                doDetailModule(request, response);
            case "/module/update":
                doUpdateModule(request, response);
            case "/module/new":
                doAddModule(request, response);
            case "/module/new/post":
                doFormAddModule(request, response);
            case "/module/delete":
                doDeleteModule(request, response);

                // Note routes
            case "/notes":
                doNotes(request, response);
            case "/note":
                doDetailNote(request, response);
            case "/note/update":
                doUpdateNote(request, response);
            case "/note/new":
                doAddNote(request, response);
            case "/note/new/post":
                doFormAddNote(request, response);
            case "/note/delete":
                doDeleteNote(request, response);

                // Redirection par défaut
            default:
                doHome(request, response);
        }
    }

    private void doHome(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        loadJSP(urlHome, request, response);
    }

    private void doEtudiants(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Etudiant> etudiants = EtudiantDAO.getAll();
        request.setAttribute("etudiants", etudiants);
        loadJSP(urlEtudiants, request, response);
    }

    private void doDetailEtudiant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int etuId = Integer.valueOf(request.getParameter("id"));
        Etudiant etu = EtudiantDAO.retrieveById(etuId);
        List<Groupe> grps = GroupeDAO.getAll();

        request.setAttribute("etudiant", etu);
        request.setAttribute("groupes", grps);

        loadJSP(urlDetailEtudiant, request, response);
    }

    private void doAddEtudiant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Groupe> grps = GroupeDAO.getAll();
        request.setAttribute("groupes", grps);
        loadJSP(urlAddEtudiant, request, response);
    }

    private void doFormAddEtudiant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"nom", "prenom", "groupe", "absences"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Etudiant etu = EtudiantDAO.create(
                paramMap.get("prenom"),
                paramMap.get("nom"),
                GroupeDAO.retrieveById(Integer.valueOf(paramMap.get("groupe"))),
                Integer.valueOf(paramMap.get("absences"))
        );
        
        request.setAttribute("id", etu.getId().toString());

        loadJSP(urlUpdateEtudiant, request, response);
    }

    private void doDeleteEtudiant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int etuId = Integer.valueOf(request.getParameter("id"));
        Etudiant etu = EtudiantDAO.retrieveById(etuId);
        EtudiantDAO.remove(etu);
        loadJSP(urlDeleteEtudiant, request, response);
    }

    private void doUpdateEtudiant(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"id", "nom", "prenom", "groupe", "absences"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Etudiant etu = EtudiantDAO.retrieveById(Integer.valueOf(paramMap.get("id")));
        etu.setNom(paramMap.get("nom"));
        etu.setPrenom(paramMap.get("prenom"));
        etu.setGroupe(GroupeDAO.retrieveById(Integer.valueOf(paramMap.get("groupe"))));
        etu.setNbAbsences(Integer.valueOf(paramMap.get("absences")));
        EtudiantDAO.update(etu);

        request.setAttribute("id", paramMap.get("id"));

        loadJSP(urlUpdateEtudiant, request, response);
    }

    private void doGroupes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Groupe> groupes = GroupeDAO.getAll();
        request.setAttribute("groupes", groupes);
        loadJSP(urlGroupes, request, response);
    }

    private void doDetailGroupe(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int grpId = Integer.valueOf(request.getParameter("id"));
        Groupe grp = GroupeDAO.retrieveById(grpId);

        request.setAttribute("groupe", grp);

        loadJSP(urlDetailGroupe, request, response);
    }

    private void doUpdateGroupe(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"id", "nom"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Groupe grp = GroupeDAO.retrieveById(Integer.valueOf(paramMap.get("id")));
        grp.setNom(paramMap.get("nom"));
        GroupeDAO.update(grp);

        request.setAttribute("id", paramMap.get("id"));

        loadJSP(urlUpdateGroupe, request, response);
    }

    private void doAbsences(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Groupe> groupes = GroupeDAO.getAll();
        request.setAttribute("groupes", groupes);
        loadJSP(urlAbsences, request, response);
    }

    private void doModules(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Module> modules = ModuleDAO.getAll();
        request.setAttribute("modules", modules);
        loadJSP(urlModules, request, response);
    }

    private void doDetailModule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int modId = Integer.valueOf(request.getParameter("id"));
        Module mod = ModuleDAO.retrieveById(modId);

        request.setAttribute("module", mod);

        loadJSP(urlDetailModule, request, response);
    }

    private void doAddModule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        loadJSP(urlAddModule, request, response);
    }

    private void doFormAddModule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"nom"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Module mod = ModuleDAO.create(paramMap.get("nom"));

        request.setAttribute("id", mod.getId().toString());

        loadJSP(urlUpdateModule, request, response);
    }

    private void doDeleteModule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int modId = Integer.valueOf(request.getParameter("id"));
        Module mod = ModuleDAO.retrieveById(modId);
        ModuleDAO.remove(mod);
        loadJSP(urlDeleteModule, request, response);
    }

    private void doUpdateModule(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"id", "nom"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Module mod = ModuleDAO.retrieveById(Integer.valueOf(paramMap.get("id")));
        mod.setName(paramMap.get("nom"));
        ModuleDAO.update(mod);

        request.setAttribute("id", paramMap.get("id"));

        loadJSP(urlUpdateModule, request, response);
    }

    private void doNotes(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Etudiant> etudiants = EtudiantDAO.getAll();
        request.setAttribute("etudiants", etudiants);
        loadJSP(urlNotes, request, response);
    }

    private void doDetailNote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int noteId = Integer.valueOf(request.getParameter("id"));
        Note note = NoteDAO.retrieveById(noteId);
        List<Etudiant> etus = EtudiantDAO.getAll();
        List<Module> modules = ModuleDAO.getAll();

        request.setAttribute("note", note);
        request.setAttribute("etudiants", etus);
        request.setAttribute("modules", modules);

        loadJSP(urlDetailNote, request, response);
    }

    private void doAddNote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Etudiant> etus = EtudiantDAO.getAll();
        List<Module> modules = ModuleDAO.getAll();
        request.setAttribute("etudiants", etus);
        request.setAttribute("modules", modules);
        loadJSP(urlAddNote, request, response);
    }

    private void doFormAddNote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"value", "etudiant", "module"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Note note = NoteDAO.create(
                Float.valueOf(paramMap.get("value")),
                EtudiantDAO.retrieveById(Integer.valueOf(paramMap.get("etudiant"))),
                ModuleDAO.retrieveById(Integer.valueOf(paramMap.get("module")))
        );

        request.setAttribute("id", note.getId().toString());

        loadJSP(urlUpdateNote, request, response);
    }

    private void doDeleteNote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int noteId = Integer.valueOf(request.getParameter("id"));
        Note note = NoteDAO.retrieveById(noteId);
        NoteDAO.remove(note);
        loadJSP(urlDeleteNote, request, response);
    }

    private void doUpdateNote(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        // Prepare a list of the keys that must be found in the form request
        String[] keys = {"id", "value", "etudiant", "module"};
        HashMap<String, String> paramMap = new HashMap<String, String>();
        // Build a mapping between the keys and the values contained in the request
        for (String key: keys) {
            String value = request.getParameter(key);
            // If a key is missing, throw an IOException
            if (value == null) {
                throw new IOException("Missing parameter \"" + key + "\" in form");
            } else {
                paramMap.put(key, value);
            }
        }

        Note note = NoteDAO.retrieveById(Integer.valueOf(paramMap.get("id")));
        Float newValue = Float.valueOf(paramMap.get("value"));
        Etudiant newEtudiant = EtudiantDAO.retrieveById(Integer.valueOf(paramMap.get("etudiant")));
        Module newModule = ModuleDAO.retrieveById(Integer.valueOf(paramMap.get("module")));

        note.setValue(newValue);
        note.setEtudiant(newEtudiant);
        note.setModule(newModule);
        NoteDAO.update(note);

        request.setAttribute("id", paramMap.get("id"));

        loadJSP(urlUpdateNote, request, response);
    }

    /**
     * Charge la JSP indiquée en paramètre
     *
     * @param url
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    public void loadJSP(String url, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // L'interface RequestDispatcher permet de transférer le contrôle à une
        // autre servlet
        // Deux méthodes possibles :
        // - forward() : donne le contrôle à une autre servlet. Annule le flux
        // de sortie de la servlet courante
        // - include() : inclus dynamiquement une autre servlet
        // + le contrôle est donné à une autre servlet puis revient à la servlet
        // courante (sorte d'appel de fonction).
        // + Le flux de sortie n'est pas supprimé et les deux se cumulent

        if (url == null) {
            throw new IOException("Loader got a null url");
        }
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher(url);
        System.out.println(request.toString() + response.toString());
        if (rd == null) {
            throw new IOException("Loader could not match url ".concat(url));
        }

        Enumeration<String> s = request.getAttributeNames();
        while (s.hasMoreElements()) {
            String elt = s.nextElement();
            System.out.println(elt + " *** " + request.getAttribute(elt));
        }

        rd.forward(request, response);
    }

}